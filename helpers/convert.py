class UNITS:

    def con_degree(self, number: int, unit: str = "celsius") -> int:
        """
        celsius = takes a celsius in float format,
        returns fahrenheit
        """
        result = 100
        if unit == "celsius":
            result = round((number * 1.8) + 32)
        elif unit == "fahrenheit":
            result = round((number - 32) * 5 / 9)
        return result

    def km_miles(self, km: int) -> int:
        """
        km = kilometers
        returns miles
        """
        return round(km * 0.6214)


if __name__ == "__main__":
    app = UNITS()
