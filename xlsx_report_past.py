import os
from datetime import datetime
from inspect import getsourcefile

import xlsxwriter


class ExportScore(object):

    row_start = 3  # The row the data stars
    date_created = datetime.now().strftime("%Y-%m-%d %I:%M%p")

    def __init__(self, past, filename):
        self.past = past
        self.filename = filename
        print(self.date_created)
        self.config()
        self.export_past()
        self.about()
        self.workbook.close()

    def config(self):
        self.workbook = xlsxwriter.Workbook(
            (self.get_resource_path(f"{self.filename}"))
        )

        self.cell_format = self.workbook.add_format(
            {
                "font_name": "Arial",
                "font_size": 11,
                "border": 1,
                "align": "center",
                "valign": "vcenter",
                "text_wrap": True,
                "shrink": True,
            }
        )

        self.header_format = self.workbook.add_format(
            {
                "font_name": "Arial",
                "bold": True,
                "font_size": 13,
                "border": 1,
                "align": "center",
                "valign": "vcenter",
                "bg_color": "#e6e6e6",
            }
        )

        self.merge_format = self.workbook.add_format(
            {
                "font_name": "Arial",
                "bold": True,
                "font_size": 18,
                "left": 1,
                "bottom": 1,
                "align": "center",
                "valign": "vcenter",
                "bg_color": "#6cc1b1",
                "text_wrap": True,
            }
        )

        self.merge_date_format = self.workbook.add_format(
            {
                "font_name": "Arial",
                "bold": True,
                "font_size": 12,
                "right": 1,
                "align": "center",
                "valign": "vcenter",
                "bg_color": "#6cc1b1",
                "text_wrap": True,
            }
        )

    def get_resource_path(self, rel_path):
        """
        requires import os , from inspect import getsourcefile
        finds the dir where this file is (absolute). returns the absolute path
        of the passed relative path.
        """
        dir_of_py_file = os.path.dirname(getsourcefile(lambda: 0))
        rel_path_to_resource = os.path.join(dir_of_py_file, rel_path)
        abs_path_to_resource = os.path.abspath(rel_path_to_resource)
        return abs_path_to_resource

    def export_past(self):

        # Some data we want to write to the worksheet.
        data = self.past

        # Create a worksheet /////////////////////////////////
        sheet_name = "Past"
        sheet = self.workbook.add_worksheet(sheet_name)

        # Sheet Options ////////////////////////////////////
        sheet.freeze_panes(self.row_start, 0)  # Freeze rows/columns
        sheet.set_column("A:E", 20)  # Column Settings
        sheet.set_selection("D4")

        # Row settings
        for i in range(len(data)):
            sheet.set_row(i + self.row_start, 50)

        # Print Formating
        sheet.set_landscape()
        sheet.set_page_view()
        sheet.fit_to_pages(1, 1)
        # game_sheet.set_print_scale(75)
        sheet.center_horizontally()
        sheet.set_paper(1)

        # Set title //////////////////////////////////////////////////
        sheet.merge_range("A1:E2", sheet_name, self.merge_format)

        # Write some data headers. ////////////////////////////////////
        sheet.write(f"A${self.row_start}", "ID", self.header_format)
        sheet.write(f"B${self.row_start}", "Icon", self.header_format)
        sheet.write(f"C${self.row_start}", "High", self.header_format)
        sheet.write(f"D${self.row_start}", "Low", self.header_format)
        sheet.write(f"E${self.row_start}", "Date", self.header_format)

        # Write Data //////////////////////////////////////////////////
        row = self.row_start
        col = 0

        for i in range(len(data)):
            # date = self.from_timestamp(ts=games[i]["date"]["$date"], format="%Y-%m-%d")
            sheet.write(row, col, data[i]["_id"]["$oid"], self.cell_format)
            sheet.write(row, col + 1, data[i]["icon"], self.cell_format)
            sheet.write(row, col + 2, data[i]["high"], self.cell_format)
            sheet.write(row, col + 3, data[i]["low"], self.cell_format)
            sheet.write(row, col + 4, data[i]["date"]["$date"], self.cell_format)

            row += 1

    def about(self):

        # Some data we want to write to the worksheet.
        past_length = len(self.past)

        # Create a worksheet /////////////////////////////////
        about_sheet_name = "About"
        about_sheet = self.workbook.add_worksheet(about_sheet_name)

        # Sheet Options ////////////////////////////////////
        about_sheet.freeze_panes(self.row_start, 0)  # Freeze rows/columns
        about_sheet.set_column(0, 3, 18, self.cell_format)  # Column Settings
        about_sheet.set_selection("D4")

        ## Row settings
        about_sheet.set_row(self.row_start, 15)

        # Print Formating
        about_sheet.set_landscape()
        about_sheet.set_page_view()
        about_sheet.fit_to_pages(1, 1)
        # about_sheet.set_print_scale(75)
        about_sheet.center_horizontally()
        about_sheet.set_paper(1)

        # Set title //////////////////////////////////////////////////
        about_sheet.merge_range(0, 0, 1, 3, about_sheet_name, self.merge_format)

        # Write some data headers. ////////////////////////////////////
        about_sheet.write(f"A${self.row_start}", "Past", self.header_format)
        about_sheet.write(f"B${self.row_start}", "Date Created", self.header_format)

        # Write Data //////////////////////////////////////////////////
        row = self.row_start
        about_sheet.write(row, 0, past_length)
        about_sheet.write(row, 1, self.date_created)

    def from_timestamp(self, ts, dt: bool = False, format: str = "%H:%M"):
        """
        ts = timestamp\n
        dt = True return datetime object,
        False return string date or time.\n
        format = format you want the string
        time or date to be in Default(%H:%M).\n
        24 hour time = '%H:%M',
        12 hour = '%I:%M %p'\n
        common date = '%Y-%m-%d' (YYYY-MM-DD)
        """
        # if type(ts) is not timestamp:
        #   return "Input is not a datetime object"
        try:
            dtout = datetime.fromtimestamp(ts)
        except Exception:
            return "Not a valid datetime"
        if dt == True:
            return dtout
        str_dt = datetime.strftime(dtout, format)
        return str_dt


if __name__ == "__main__":
    app = ExportScore()
