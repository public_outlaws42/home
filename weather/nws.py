#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This is a weather module for the National Weather Service(USA) API
"""
import logging

# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

import requests

# import geopy.geocoders
from geopy.geocoders import Nominatim

# Import helpers from this project
from helpers.convert import UNITS
from helpers.dict_list import DictList
from helpers.dt import DT
from helpers.io import IO

__author__ = "Troy Franks"
__version__ = "2024-11-23.1.0"

logging.basicConfig(
    filename="wu.log",
    level=logging.INFO,
    format="%(asctime)s %(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
)

io = IO()
dl = DictList()
un = UNITS()
dt = DT()


class Weather:
    """
    Get current and forecast from the National Weather Service(US)
    """

    degree_sign = "\N{DEGREE SIGN}"

    def __init__(self):
        pass

    def get_forecast(self):
        """
        combine all the different forecast dictionaries into one list.
        """
        forecast_l = []
        dl.add_to_list(self.forecast_dow(), forecast_l)
        dl.add_to_list(self.forecast_temp(), forecast_l)
        dl.add_to_list(self.forecast_precip_day(), forecast_l)
        dl.add_to_list(self.forecast_code(), forecast_l)
        dl.add_to_list(self.forecast_datetime(), forecast_l)
        return forecast_l

    def geolocation(self, address):
        """
        Take a zip code and get the longitude and latitude
        """
        try:
            geolocator = Nominatim(user_agent="weather")
            location = geolocator.geocode(address)
            addressout = location.address
            addresslist = addressout.split(",")
            if len(address) <= 5:
                city = addresslist[0]
            else:
                city = addresslist[2]
            return location.latitude, location.longitude, city
        except Exception:
            return 41.232921, -85.649106, "columbia city"

    def get_weather_info(self, use_api: bool, zip_code: str, units: str) -> None:
        """
        Collects the weather info from the service api then it stores
        it in a json file locally.
        requires imports: requests
        """
        for_file = "forecast.json"
        cur_file = "current.json"
        ss_file = "ss.json"
        weather_station = "KFWA"  # "KASW"
        wfo = "IWX"
        x = "69"
        y = "38"
        # location = self.geolocation(ZIP_CODE)
        # lat, long, self.city = location
        # lat = 41.232921
        # long = -85.649106
        try:
            if use_api == True:
                c = requests.get(
                    f"https://api.weather.gov/stations/{weather_station}/observations/latest"
                )
                f = requests.get(
                    f"https://api.weather.gov/gridpoints/{wfo}/{x},{y}/forecast"
                )
                ss = requests.get(
                    "https://api.sunrise-sunset.org/json?lat=41.23921&lng=-85.649106"
                )

                sun = ss.json()
                current = c.json()
                forecast = f.json()
                if (
                    current["properties"]
                    and current["properties"]["temperature"]["value"] is not None
                ):
                    io.save_json(cur_file, current, "relative")
                else:
                    print("No values")
                # io.save_json(cur_file, current, "relative")
                io.save_json(ss_file, sun, "relative")

                if forecast["properties"]:
                    io.save_json(for_file, forecast, "relative")
                else:
                    print(forecast["status"])
                self.current = io.open_json(cur_file, "relative")
                self.sun = io.open_json(ss_file, "relative")
                self.forecast_in = io.open_json(for_file, "relative")
            else:
                self.current = io.open_json(cur_file, "relative")
                self.sun = io.open_json(ss_file, "relative")
                self.forecast_in = io.open_json(for_file, "relative")
        except Exception as e:
            self.current = io.open_json(cur_file, "relative")
            self.sun = io.open_json(ss_file, "relative")
            self.forecast_in = io.open_json(for_file, "relative")
            print(f"Collect current error: {str(e)}")
            logging.info("Collect current error:  " + str(e))
            pass

    def gleen_info(self):
        """
        Gets the current weather attributes from the json file.
        """
        # weather service
        # left weather info
        # brief description of the weather
        status = (
            {"current_status": self.current["properties"]["textDescription"]}
            if self.current["properties"]["textDescription"] is not None
            else "Clear"
        )
        description = (
            {"current_description": self.current["properties"]["textDescription"]}
            if self.current["properties"]["textDescription"] is not None
            else "Clear"
        )
        city = {"current_city": "Warsaw"}
        timezone_hour = {"current_timezone": "12"}  # Hours from UTC

        # outside temp .
        temp_c: int = round(self.current["properties"]["temperature"]["value"])
        outdoor_temp = {"current_temp": un.con_degree(number=temp_c)}

        # wind
        try:
            import_wind_dir = (
                self.current["properties"]["windDirection"]["value"]
                if self.current["properties"]["windDirection"]["value"] is not None
                else 0
            )
        except TypeError:
            import_wind_dir = 0

        wind_dir = {"current_wind_dir": self.degtocompass(import_wind_dir)}
        wind_speed_km = (
            round(self.current["properties"]["windSpeed"]["value"])
            if self.current["properties"]["windSpeed"]["value"] is not None
            else 0
        )
        wind_speed = {"current_wind_speed": round(un.km_miles(wind_speed_km))}
        try:
            wind_gust_km = round(self.current["properties"]["windGust"]["value"])
            wind_gust = {"current_wind_gust": round(un.km_miles(wind_gust_km))}
        except TypeError:
            wind_gust = {"current_wind_gust": 0}

        # Humidity
        try:
            humidity = {
                "current_humidity": f"{round(self.current['properties']['relativeHumidity']['value'])}%"
            }
        except TypeError:
            humidity = {"current_humidity": 0}

        # Atmospheric Pressure
        try:
            pressure = {
                "current_pressure": self.current["properties"]["barometricPressure"][
                    "value"
                ]
            }  #  hPa
        except TypeError:
            pressure = {"current_pressure": 0}
        # Feels Like
        try:
            wind_chill = round(self.current["properties"]["windChill"]["value"])
            feels_like = {"current_feels_like": un.con_degree(wind_chill)}
        except TypeError:
            feels_like = {"current_feels_like": outdoor_temp["current_temp"]}

        # Sun Rise
        sr_timestamp = dt.from_str_time(
            str_time=self.sun["results"]["sunrise"],
            utc=False,
            timestamp=True,
            format="%I:%M:%S %p",
        )
        sr_datetime = dt.utc_to_localtime(sr_timestamp)
        sr = dt.from_datetime(dt=sr_datetime, format="%I:%M %p", timestamp=True)
        sun_rise = {"current_sunrise": sr}

        # Sun Set
        ss_timestamp = dt.from_str_time(
            str_time=self.sun["results"]["sunset"],
            utc=False,
            timestamp=True,
            format="%I:%M:%S %p",
        )
        ss_datetime = dt.utc_to_localtime(ss_timestamp)
        ss = dt.from_datetime(dt=ss_datetime, format="%I:%M %p", timestamp=True)
        sun_set = {"current_sunset": ss}

        # Visibility
        try:
            visibility_km = round(self.current["properties"]["visibility"]["value"])
            visibility = {"visibility": round(un.km_miles(visibility_km))}
        except TypeError:
            visibility = {"visibility": 10}

        # Current Icon
        current_icon = {
            "current_icon": self.condition_code_from_description(
                description=status["current_status"]
            )
        }
        updated = self.current["id"]
        update_time = updated[62:70]
        refresh = {
            "updated": dt.from_str_time(
                str_time=update_time,
                format="%H:%M:%S",
                utc=True,
            )
        }

        return [
            status,
            description,
            city,
            timezone_hour,
            outdoor_temp,
            refresh,
            wind_dir,
            wind_speed,
            wind_gust,
            humidity,
            pressure,
            feels_like,
            current_icon,
            sun_rise,
            sun_set,
            visibility,
        ]

    def degtocompass(self, degrees):
        """
        Returns the string wind direction for the degrees supplied
        """
        direction = [
            "N",
            "NNE",
            "NE",
            "ENE",
            "E",
            "ESE",
            "SE",
            "SSE",
            "S",
            "SSW",
            "SW",
            "WSW",
            "W",
            "WNW",
            "NW",
            "NNW",
            "N",
        ]
        val = int((degrees / 22.5) + 0.5)
        return direction[(val % 16)]

    def forecast_days(self, days: int = 3) -> list:
        """
        returns the first 3 letters abbr of forecast days of the week
        This uses  2 counters. one to control how man days.
        The other is for naming of the key in the dictionary so
        it is the same across the board.
        default: it will get 3 days
        """
        forecast_day = []
        daytime_counter = 0
        key_counter: int = 0
        len_periods: int = len(self.forecast_in["properties"]["periods"])
        for i in range(len_periods):
            daytime = self.forecast_in["properties"]["periods"][i]["isDaytime"]
            if daytime is True:
                daytime_counter += 1
                if daytime_counter <= days:
                    day_long = self.forecast_in["properties"]["periods"][i]["name"]
                    day_short = day_long[:3]
                    day = {f"day{key_counter}_dow": day_short}
                    key_counter += 1
                    forecast_day.append(day)

        return forecast_day

    def forecast_dow(self, days: int = 3):
        """
        Gets the first 3 letter abbreviation of the days for the forecast
        must from datetime import datetime, timedelta
        default: is 3 days
        """
        forecast_days = []
        for i in range(days):
            day = datetime.today() + timedelta(i)
            forecast_days.append({f"day{i}_dow": day.strftime("%a")})
        return forecast_days

    def forecast_temp(self, days: int = 3) -> list:
        """
        returns the high and low temperatures for the
        amount of days requested.
        This uses  3 counters. one to control how man days.
        The others are for naming of the keys in the dictionaries so
        it is the same across the board.
        By default it would be 3
        """
        forecast: list = []
        daytime_counter: int = 0
        nighttime_counter: int = 0
        day_key_c: int = 0
        night_key_c: int = 0
        len_periods: int = len(self.forecast_in["properties"]["periods"])
        for i in range(len_periods):
            name = self.forecast_in["properties"]["periods"][i]["name"]
            daytime = self.forecast_in["properties"]["periods"][i]["isDaytime"]
            if daytime is True:
                daytime_counter += 1
                if daytime_counter <= days:

                    temp = {
                        f"day{day_key_c}_temp_high": round(
                            self.forecast_in["properties"]["periods"][i]["temperature"]
                        )
                    }
                    day_key_c += 1
                    forecast.append(temp)
            elif daytime is False and name != "Overnight":
                nighttime_counter += 1
                if nighttime_counter <= days:
                    temp = {
                        f"day{night_key_c}_temp_low": round(
                            self.forecast_in["properties"]["periods"][i]["temperature"]
                        ),
                    }
                    night_key_c += 1
                    forecast.append(temp)

            # forecast.append(temp)
        return forecast

    def forecast_code(self, days: int = 3):
        """
        returns a dictionary list of the icon codes
        based on the weather condition.
        This uses  2 counters. one to control how man days.
        The other is for naming of the key in the dictionary so
        it is the same across the board.
        default it will get three days.
        """
        # forecast code is day / night key word starting at index 0 for 3 days
        forecast_day_code = []
        daytime_counter: int = 0
        key_counter: int = 0
        len_periods: int = len(self.forecast_in["properties"]["periods"])
        for i in range(len_periods):
            daytime: bool = self.forecast_in["properties"]["periods"][i]["isDaytime"]
            if daytime is True:
                daytime_counter += 1
                if daytime_counter <= days:
                    temp = {
                        f"day{key_counter}_icon": self.condition_code_from_description(
                            description=self.forecast_in["properties"]["periods"][i][
                                "shortForecast"
                            ],
                        )
                    }

                    key_counter += 1
                    forecast_day_code.append(temp)
        return forecast_day_code

    def forecast_precip_day(self, days: int = 3) -> list:
        """
        returns a dictionary list of the chances of percipitation
        This uses  2 counters. one to control how man days.
        The other is for naming of the key in the dictionary so
        it is the same across the board.
        default: it will get 3 days.
        """
        # pop is day night chance of precip starting at index 0 for 3 days
        forecast_pr: list = []
        daytime_counter: int = 0
        key_counter: int = 0
        len_periods: int = len(self.forecast_in["properties"]["periods"])
        for i in range(len_periods):
            daytime: bool = self.forecast_in["properties"]["periods"][i]["isDaytime"]
            if daytime is True:
                daytime_counter += 1
                if daytime_counter <= days:
                    temp = self.forecast_in["properties"]["periods"][i][
                        "probabilityOfPrecipitation"
                    ]["value"]
                    if temp is not None:
                        temp_calc = float(temp)
                        # temp_calc = float(temp) * 100
                    else:
                        temp_calc = 0
                    forecast_pr.append({f"day{key_counter}_pop": f"{int(temp_calc)}%"})
                    key_counter += 1
        return forecast_pr

    def forecast_datetime(self):
        # pop is day night chance of precip starting at index 0 for 3 days
        forecast_dt = [{"date": datetime.utcnow(), "replace": 1}]
        return forecast_dt

    def condition_code_from_description(self, description: str) -> int:
        """
        Takes the string short description that is provided
        by the NWS and translates it into the number icon
        code used by the program.
        """
        clear = ["sunny", "clear", "sun"]
        partly = ["partly", "mostly"]
        rain = ["rain", "drizzle", "hail", "ice"]
        snow = ["snow"]
        cloudy = ["overcast"]
        fog = ["fog"]
        ad_code = 800
        if any(i in description.lower() for i in clear):
            ad_code = 800
        elif any(i in description.lower() for i in fog):
            ad_code = 741
        elif any(i in description.lower() for i in cloudy):
            ad_code = 804
        elif any(i in description.lower() for i in snow):
            ad_code = 601
        elif any(i in description.lower() for i in rain):
            ad_code = 501
        elif any(i in description.lower() for i in partly):
            ad_code = 801
        return ad_code


if __name__ == "__main__":
    app = Weather()
